import {Column, Entity, PrimaryGeneratedColumn, Unique} from 'typeorm';
import {IsInt, IsNumberString, IsOptional, IsPositive, MaxLength} from "class-validator";

@Entity({name: "sportsmen"})
export class SportsmenEntity {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({nullable: true})
    @IsOptional()
    @MaxLength(5)
    fullname!: string;

    @Column({nullable: true})
    @IsOptional()
    gender!: string;

    @Column({nullable: true})
    @IsOptional()
    @IsPositive()
    @IsInt()
    age!: number;

    @Column({nullable: true})
    @IsOptional()
    @IsPositive()
    @IsInt()
    height!: number;

    @Column({nullable: true})
    @IsOptional()
    @IsPositive()
    @IsInt()
    weight!: number;

    @Column({nullable: true})
    @IsOptional()
    team!: string;

    @Column({nullable: true})
    @IsOptional()
    noc!: string;

    @Column({nullable: true})
    @IsOptional()
    games!: string;

    @Column({nullable: true})
    @IsOptional()
    @IsPositive()
    @IsInt()
    year!: number;

    @Column({nullable: true})
    @IsOptional()
    season!: string;

    @Column({nullable: true})
    @IsOptional()
    city!: string;

    @Column({nullable: true})
    @IsOptional()
    sport!: string;

    @Column({nullable: true})
    @IsOptional()
    event!: string;

    @Column({nullable: true})
    @IsOptional()
    medal!: string;

}
