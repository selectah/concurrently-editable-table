import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import {SportsmenEntity} from "../../entity/sportsmen.entity";
import {SportsmenService} from "./sportsmen.service";
import {SportsmenController} from "./sportsmen.controller";

@Module({
    imports: [TypeOrmModule.forFeature([SportsmenEntity])],
    providers: [SportsmenService],
    controllers: [SportsmenController],
})
export class SportsmenModule {}
