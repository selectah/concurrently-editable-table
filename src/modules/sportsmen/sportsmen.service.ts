import {Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {SportsmenEntity} from "../../entity/sportsmen.entity";
import {Repository, UpdateResult} from "typeorm";
import {InsertResult} from "typeorm/query-builder/result/InsertResult";

@Injectable()
export class SportsmenService {

    constructor(
        @InjectRepository(SportsmenEntity)
        private sportsmenRepository: Repository<SportsmenEntity>
    ) {

    }

    async getSportsmen(): Promise<SportsmenEntity[]> {
        return await this.sportsmenRepository.find({
            order: {
                id: "ASC"
            }
        });
    }

    async addSportsman(): Promise<SportsmenEntity> {
        const insertResult: InsertResult = await this.sportsmenRepository.insert({});
        const sportsman = await this.sportsmenRepository.findOne(insertResult.raw[0].id)
        return sportsman
    }

    async updateSportsman(id: string, sportsmanPartial: SportsmenEntity): Promise<UpdateResult> {
        const updateResult: UpdateResult = await this.sportsmenRepository.update(id, sportsmanPartial);
        return updateResult
    }

    async deleteSportsman(id: string): Promise<void> {
        await this.sportsmenRepository.delete(id);
        return;
    }
}
