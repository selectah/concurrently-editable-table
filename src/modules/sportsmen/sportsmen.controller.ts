import {Body, Controller, Delete, Get, HttpCode, Param, Post, Put, UsePipes, ValidationPipe} from '@nestjs/common';
import {SportsmenService} from './sportsmen.service';
import {SportsmenEntity} from "../../entity/sportsmen.entity";

@Controller('sportsmen')
export class SportsmenController {
    constructor(private readonly sportsmenService: SportsmenService) {
    }

    @Get()
    async getSportsmen(): Promise<SportsmenEntity[]> {
        const sportsmen = await this.sportsmenService.getSportsmen();
        return sportsmen
    }

    @Put(':id')
    @HttpCode(204)
    @UsePipes(new ValidationPipe({transform: true, transformOptions: {enableImplicitConversion: true}}))
    async updateSportsmen(
        @Param('id') id: string,
        @Body() body: SportsmenEntity
    ): Promise<void> {
        await this.sportsmenService.updateSportsman(id, body);
        return;
    }

    @Post()
    async addSportsmen(): Promise<SportsmenEntity> {
        const sportsmen = await this.sportsmenService.addSportsman();
        return sportsmen
    }

    @Delete(':id')
    @HttpCode(204)
    async deleteSportsman(@Param('id') id: string): Promise<void> {
        await this.sportsmenService.deleteSportsman(id);
        return;
    }
}
