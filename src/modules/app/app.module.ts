import {Module} from '@nestjs/common';
import {ServeStaticModule} from '@nestjs/serve-static';
import {join} from 'path';
import {EventsGateway} from './events.gateway';
import {ConfigModule, ConfigService} from '@nestjs/config';
import {SportsmenEntity} from "../../entity/sportsmen.entity";
import {TypeOrmModule} from "@nestjs/typeorm";
import {SportsmenModule} from "../sportsmen/sportsmen.module";
import {AppController} from "./app.controller";
import {AppService} from "./app.service";

@Module({
    imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule, SportsmenModule],
            useFactory: (configService: ConfigService) => ({
                type: 'postgres',
                host: configService.get('DB_HOST'),
                port: configService.get('DB_PORT'),
                database: configService.get('DB_NAME'),
                username: configService.get('DB_USER'),
                password: configService.get('DB_PASSWORD'),
                entities: [SportsmenEntity],
                synchronize: true,
            }),
            inject: [ConfigService],
        }),
        ServeStaticModule.forRoot({
            rootPath: join(process.cwd(), 'front/dist'),
            renderPath: '/'
        }),
        SportsmenModule,
    ],
    providers: [AppService, EventsGateway],
    controllers: [AppController]
})
export class AppModule {}
