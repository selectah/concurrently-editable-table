import {Injectable, Logger} from "@nestjs/common";
import {CColours} from "./colours.constant";


@Injectable()
export class AppService {
    private availableColoursMap = new Map<string, boolean>();
    private idToColourMap = new Map<string, string>();
    private logger: Logger = new Logger('AppService');

    constructor() {
        CColours.forEach(colour => this.availableColoursMap.set(colour, true))
    }

    getColour(socketId): string {
        const colourEntry =  Array.from(this.availableColoursMap.entries())
            .find(entry => entry[1]);
        this.availableColoursMap.set(colourEntry[0], false);
        this.idToColourMap.set(socketId, colourEntry[0]);
        return colourEntry[0];
    }

    releaseColour(socketId) {
        const colour = this.idToColourMap.get(socketId);
        this.availableColoursMap.set(colour, true);
        this.idToColourMap.delete(socketId);
    }

    takeColour(colour, socketId) {
        this.availableColoursMap.set(colour, false);
        this.idToColourMap.set(socketId, colour);
    }

    getColoursList(): string[] {
        return  Array.from(this.availableColoursMap.entries())
            .filter(entry => !entry[1])
            .map(entry => entry[0]);
    }

    getColourById(socketId){
        console.log(socketId, this.idToColourMap.get(socketId))
        return this.idToColourMap.get(socketId);
    }
}