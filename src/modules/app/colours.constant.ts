export const CColours = [
    'black',
    'yellow',
    'green',
    'red',
    'blue',
    'purple',
    'pink',
    'indigo',
    'orange',
    'brown',
    'grey',
    'cyan',
    'teal',
    'amber',
    'lime'
];
