import {
    SubscribeMessage,
    WebSocketGateway,
    OnGatewayInit,
    WebSocketServer,
    OnGatewayConnection,
    OnGatewayDisconnect,
} from '@nestjs/websockets';
import {Logger} from '@nestjs/common';
import {Socket, Server} from 'socket.io';

import {AppService} from "./app.service";

export enum ECommands {
    changeCellValue = 'changeCellValue',
    enableCell = 'enableCell',
    disableCell = 'disableCell',
    addSportsman = 'addSportsman',
    deleteSportsman = 'deleteSportsman',
}

export interface IEventFocusCellPayload {
    id: number,
    propName: string
}

export interface IEventData {
    name: string,
    payload: IEventFocusCellPayload,

}

@WebSocketGateway({
    path: '/api/events',
    cors: {
        origin: '*',
    },
})
export class EventsGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

    constructor(private readonly appService: AppService) {
    }

    @WebSocketServer() server: Server;
    private logger: Logger = new Logger('EventsGateway');

    @SubscribeMessage('event')
    handleMessage(client: Socket, data: IEventData): void {
        client.broadcast.emit('event', data);
    }

    afterInit(server: Server) {
        this.logger.log('Initialized.');
    }

    handleDisconnect(client: Socket) {
        client.broadcast.emit('toggleColour',
            {
                active: false,
                colour: this.appService.getColourById(client.id)
            }
        );
        this.appService.releaseColour(client.id);

        this.logger.log(`Client disconnected - ${client.id}.`)
    }

    handleConnection(client: Socket) {
        this.logger.log(`Client connected - ${client.id}.`)
    }
}