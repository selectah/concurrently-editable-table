import {
    Body,
    Controller, Delete,
    Get, Param, Put
} from '@nestjs/common';
import {AppService} from "./app.service";


@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {
    }

    @Put('colours')
    async set(@Body('colour') colour: string, @Body('socketId') socketId: string ) {
        return this.appService.takeColour(colour, socketId);
    }

    @Get('colours')
    async list() {
        return this.appService.getColoursList();
    }

    @Get('colours/:socketId')
    async getColour(@Param('socketId') socketId: string) {
        return this.appService.getColour(socketId);
    }
}
