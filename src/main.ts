import {NestFactory} from '@nestjs/core';
import {AppModule} from "./modules/app/app.module";

async function bootstrap() {
    console.log(process.env.API_PORT, process.env.API_HOST);
    const app = await NestFactory.create(AppModule,
        {
            cors: {
                origin: process.env.ALLOWED_ORIGIN,
                credentials: true,
                methods: ['GET', 'POST', 'PUT', 'DELETE']
            }
        }
    );
    app.setGlobalPrefix('api')
    await app.listen(process.env.API_PORT);
}

bootstrap();
