FROM node:lts-alpine As builder
WORKDIR /cet
COPY package.json ./
RUN npm install -g rimraf
RUN npm install --only=development
COPY . .
RUN npm run build

FROM node:lts-alpine As build
ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}
WORKDIR /cet
COPY package*.json ./
RUN npm install --only=production

COPY --from=builder /cet/dist/ ./dist
EXPOSE 3000
CMD ["node", "dist/main"]
