import 'vuetify/styles';
import '@mdi/font/css/materialdesignicons.css';
import {createApp} from 'vue';
import {createVuetify} from 'vuetify';
import App from '@/App';
import store from '@/common/app-store';
import socket from "@/common/socket";
import init from "@/common/init";
// import useVuelidate from "@vuelidate/core";

const app = createApp(App);
app.use(store);
// app.use(useVuelidate());
app.use(createVuetify({}));

socket.on('connect', async () => {
    try {
        await init(socket.id);
        app.mount('#app');
    } catch (error) {
        alert(error.message);
    }
});

