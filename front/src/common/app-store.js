import createPersistedState from "vuex-persistedstate";
import {Store} from "vuex";
import {storeActions} from "@/common/constants";

const settingsState = createPersistedState({
    storage: window.sessionStorage,
    key: 'colour',
    reducer: state => ({colour: state.colour}),
})

const sportsmenState = createPersistedState({
    storage: window.localstorage,
    key: 'sportsmen',
    reducer: state => ({sportsmen: state.sportsmen}),
})

export default new Store({
    state() {
        return {
            colour: '',
            sportsmen: []
        }
    },
    mutations: {
        updateSportsmanProperty(state, payload) {
            const index = state.sportsmen.indexOf(sportsman => sportsman.id == payload.id);
            state.sportsmen[index][payload.propName] = payload.value;
        },
        deleteSportsmen(state, id) {
            const index = state.sportsmen.indexOf(sportsman => sportsman.id == id);
            state.sportsmen.splice(index, 1);
        },
        setSportsmenList(state, sportsmen) {
            state.sportsmen = sportsmen;
        },
        addSportsman(state, sportsman) {
            state.sportsmen.push(sportsman);
        },
        setColour(state, colour) {
            state.colour = colour;
        }
    },
    actions: {
        updateSportsmanProperty(context, payload) {
            context.commit(storeActions.updateSportsmanProperty, payload);
        },
        deleteSportsman(context, id) {
            context.commit(storeActions.deleteSportsman, id)
        },
        addSportsman(context, sportsman) {
            context.commit(storeActions.addSportsman, sportsman);
        },
        setSportsmenList(context, sportsmen) {
            context.commit(storeActions.setSportsmenList, sportsmen);
        },
        setColour(context, colour) {
            context.commit(storeActions.setColour, colour);
        }
    },
    getters: {
        sportsmen(state) {
            return state.sportsmen;
        },
        colour(state) {
            return state.colour;
        },
    },
    plugins: [settingsState, sportsmenState]
});
