import axios from 'axios'

const api = axios.create({
    baseURL: process.env['VUE_APP_API_HOST'] + process.env['VUE_APP_API_PATH'],
    withCredentials: true,
    headers: {
        'Accept': '*/*',
        'Content-Type': 'application/json'
    }
})

export default {
    getColoursList() {
        return api.get(`/colours`).then(response => response.data);
    },
    getColour(socketId) {
        return api.get(`/colours/${socketId}`).then(response => response.data);
    },
    setColour(colour, socketId) {
        return api.put('/colours', {colour, socketId});
    },
    getSportsmen() {
        return api.get('/sportsmen').then(response => response.data);
    },
    addSportsman() {
        return api.post('/sportsmen').then(response => response.data);
    },
    updateSportsman(id, sportsman) {
        return api.put(`/sportsmen/${id}`, sportsman);
    },
    deleteSportsman(id) {
        return api.delete(`/sportsmen/${id}`);
    }
}