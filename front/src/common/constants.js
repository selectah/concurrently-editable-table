export const headers = [
    'id',
    'fullname',
    'gender',
    'age',
    'height',
    'weight',
    'team',
    'noc',
    'games',
    'year',
    'season',
    'city',
    'sport',
    'event',
    'medal'
];

export const storeActions = {
    addSportsman: 'addSportsman',
    deleteSportsman: 'deleteSportsman',
    updateSportsmanProperty: 'updateSportsmanProperty',
    setSportsmenList: 'setSportsmenList',
    setColour: 'setColour'

};

export const socketEvents = {
    updateSportsman: 'updateSportsman',
    addSportsman: 'addSportsman',
    deleteSportsman: 'deleteSportsman',
    updateCell: 'updateCell',
    toggleColour: 'toggleColour',
}