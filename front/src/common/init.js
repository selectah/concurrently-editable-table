import api from "@/common/api";
import store from "@/common/app-store";

export default async (socketId) => {
    try {
        const clientColour = store.getters.colour;
        if(!clientColour){
            const availableColour = await api.getColour(socketId);
            store.dispatch('setColour', availableColour);
        } else {
            await api.setColour(clientColour, socketId);
        }

        const sportsmen = await api.getSportsmen();
        store.dispatch('setSportsmenList', sportsmen);
    } catch (error) {
        alert(error.message);
    }

}