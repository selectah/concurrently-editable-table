import io from 'socket.io-client'

const socket = io(process.env['VUE_APP_API_HOST'],
    {
        path: process.env['VUE_APP_SOCKET_PATH'],
        transport: ["websocket", "polling"]
    }
);

socket.on('connect', async () => {
    console.log(`Socket ID - ${socket.id}`)
});

export default socket;