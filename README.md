## Concurrently editable table

### Start for production

###### Requirements
- Docker compose v1.29.2

###### Installation
```bash
git clone https://selectah@bitbucket.org/selectah/concurrently-editable-table.git
cd concurrently-editable-table
docker-compose up -d
```

###### Web link

[http://localhost:8080](https://localhost:8080)

### Start for development

###### Requirements

- NodeJS v16.14.2
- Docker compose v1.29.2

###### Installation

```bash
git clone https://selectah@bitbucket.org/selectah/concurrently-editable-table.git
cd concurrently-editable-table
npm install 
cd front && npm install
```

###### Run for development
```bash
npm run start:dev
cd front && npm run watch
```

###### Web link

[http://localhost:3000](https://localhost:3000)
